#include <iostream>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>

#include "mapra_test.h"

#include "matrix.h"
#include "vector.h"

void TestMatrixVector() {
	mapra::MapraTest test("Matrix Tester");

	mapra::Matrix matrix1(8,9);
	mapra::Matrix matrix2(8,9);
	mapra::Vector vector1(9);
	mapra::Vector vector2(8);

	matrix1(1, 2) = 2;
	matrix2(1, 2) = 2;

	vector1(0) = 3;
	vector1(1) = 2;
	vector1(2) = 1;

	vector2(1) = 2;

	test.AssertEq("Matrizen gleich", matrix1, matrix2);
	test.AssertEq("Matrix-Vektor-Produkt", matrix1 * vector1, vector2);

	matrix1.Redim(2, 2);
	matrix2.Redim(2, 2);
	test.AssertEq("Redim Rows", matrix1.GetRows(), (std::size_t)2);
	test.AssertEq("Redim Cols", matrix1.GetCols(), (std::size_t)2);

	test.AssertEq("Matrix-Matrix-Produkt", matrix1 * matrix2, matrix1);

	test.AssertEq("Zugriff Matrix", matrix1(1, 1), 0.0);
	test.AssertEq("Zugriff Vektor", vector1(2),    1.0);
}

int main() {
	TestMatrixVector();
	return 0;
}
