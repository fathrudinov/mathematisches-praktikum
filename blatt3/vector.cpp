// Copyright (c) 2022, The MaPra Authors.

#include "vector.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>
#include <iomanip>

// =======================
//      Konstruktoren
// =======================

// ----- Konstruktor -----

mapra::Vector::Vector(std::size_t len) : elems_(len, 0) {}

mapra::Vector::Vector(const mapra::Vector& vector) : elems_(vector.GetLength(), 0) {
	(*this) = vector;
}

// ===========================================
//      Vektorelement schreiben und lesen
// ===========================================

// ----- Schreib- und Lesezugriff auf Vektorelemente -----

double& mapra::Vector::operator()(std::size_t i) {
#ifndef NDEBUG
	if (i >= (*this).GetLength()) {
		mapra::Vector::VecError("Ungueltiger Index!");
	}
#endif

	return elems_[i];
}

// ----- Lesezugriff auf Elemente konstanter Vektoren -----

double mapra::Vector::operator()(std::size_t i) const {
#ifndef NDEBUG
	if (i >= (*this).GetLength()) {
		mapra::Vector::VecError("Ungueltiger Index!");
	}
#endif

	return elems_[i];
}

// =====================
//      Zuweisungen
// =====================

// ----- Zuweisungsoperator "=" ----

mapra::Vector& mapra::Vector::operator=(const mapra::Vector& other) {
	if ((*this).GetLength() != other.GetLength())
		Redim(other.GetLength());

	for (std::size_t i = 0; i < (*this).GetLength(); i++)
		(*this)(i) = other(i);

	return *this;
}

// ----- Zuweisungsoperator mit Addition "+=" ----

mapra::Vector& mapra::Vector::operator+=(const mapra::Vector& other) {
#ifndef NDEBUG
	if ((*this).GetLength() != other.GetLength()) {
		mapra::Vector::VecError("Inkompatible Dimensionen fuer 'Vektor += Vektor'!");
	}
#endif

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) += other(i);
	}

	return *this;
}

// ----- Zuweisungsoperator mit Subtraktion "-=" ----

mapra::Vector& mapra::Vector::operator-=(const mapra::Vector& other) {
#ifndef NDEBUG
	if ((*this).GetLength() != other.GetLength()) {
		mapra::Vector::VecError("Inkompatible Dimensionen fuer 'Vektor -= Vektor'!");
	}
#endif

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) -= other(i);
	}

	return *this;
}

// ----- Zuweisungsoperator mit Multiplikation "*=" ----

mapra::Vector& mapra::Vector::operator*=(double c) {
	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) *= c;
	}

	return *this;
}

// ----- Zuweisungsoperator mit Divsion "/=" ----

mapra::Vector& mapra::Vector::operator/=(double c) {
	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) /= c;
	}

	return *this;
}

// ==============================
//      Vektorlaenge aendern
// ==============================

// ----- Vektorlaenge aendern -----

mapra::Vector& mapra::Vector::Redim(std::size_t l) {
	elems_ = std::vector<double>(l, 0);
	return *this;
}

std::size_t mapra::Vector::GetLength() const { return elems_.size(); }

// ======================
//      Vektornormen
// ======================

// ----- Euklidische Norm -----

double mapra::Vector::Norm2() const {
	double res = 0;

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		res += (*this)(i) * (*this)(i);
	}

	return std::sqrt(res);
}

// ----- Maximum-Norm -----

double mapra::Vector::NormMax() const {
	double highscore = std::abs((*this)(0));

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		if (highscore <= std::abs((*this)(i)))
			highscore = std::abs((*this)(i));
	}

	return highscore;
}

// ==================================
//      arithmetische Operatoren
// ==================================

// ----- Addition "+" -----

mapra::Vector mapra::operator+(const mapra::Vector& x, const mapra::Vector& y) {
#ifndef NDEBUG
	if (x.GetLength() != y.GetLength()) {
		mapra::Vector::VecError("Inkompatible Dimensionen fuer 'Vektor + Vektor'!");
	}
#endif

	mapra::Vector z = x;
	return z += y;
}

// ----- Subtraktion "-" -----

mapra::Vector mapra::operator-(const mapra::Vector& x, const mapra::Vector& y) {
#ifndef NDEBUG
	if (x.GetLength()  != y.GetLength()) {
		mapra::Vector::VecError("Inkompatible Dimensionen fuer 'Vektor + Vektor'!");
	}
#endif

	mapra::Vector z = x;
	return z -= y;
}

// ----- Vorzeichen wechseln "-" -----

mapra::Vector mapra::operator-(const mapra::Vector& x) {
	mapra::Vector z = Vector(x.GetLength());

	return z - x;
}

// ----- Skalarprodukt "*" -----

double mapra::operator*(const mapra::Vector& x, const mapra::Vector& y) {
#ifndef NDEBUG
	if (x.GetLength() != y.GetLength()) {
		mapra::Vector::VecError("Skalarprodukt ungleiche Vektorlaenge");
	}
#endif

	double res = 0;

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		res += x(i) * y(i);
	}

	return res;
}

// ----- Multiplikation Skalar*Vektor "*" -----

mapra::Vector mapra::operator*(double c, const mapra::Vector& x) {
	mapra::Vector z = x;
	return z *= c;
}

// ----- Multiplikation Vektor*Skalar "*" -----

mapra::Vector mapra::operator*(const mapra::Vector& x, double c) {
	return c * x;
}

// ----- Division Vektor/Skalar "/" -----

mapra::Vector mapra::operator/(const mapra::Vector& x, double c) {
	mapra::Vector z = x;
	return z /= c;
}

// ==============================
//      Vergleichsoperatoren
// ==============================

// ----- Test auf Gleichheit "==" -----

bool mapra::operator==(const mapra::Vector& x, const mapra::Vector& y) {
	if (x.GetLength() != y.GetLength()) {
		return false;
	}

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		if (x(i) != y(i)) {
			return false;
		}
	}

	return true;
}

// ----- Test auf Ungleichheit "!=" -----

bool mapra::operator!=(const mapra::Vector& x, const mapra::Vector& y) {
	return !(x == y);
}

// ==========================
//      Ein- und Ausgabe
// ==========================

// ----- Ausgabe "<<" -----

std::ostream& mapra::operator<<(std::ostream& s, const mapra::Vector& x) {
	s << std::setiosflags(std::ios::right);

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		s << "\n(" << std::setw(4) << i << ") " << x(i);
	}

	return s << std::endl;
}

// ----- Eingabe ">>" -----

std::istream& mapra::operator>>(std::istream& s, mapra::Vector& x) {
	std::cout << std::setiosflags(std::ios::right);

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		std::cout << "\n(" << std::setw(4) << i << ") " << std::flush;
		s >> x(i);
	}

	return s;
}

// ==========================
//      Fehlerbehandlung
// ==========================

// ----- Ausgabe der Fehlermeldung "str" -----

void mapra::Vector::VecError(const char str[]) {
	std::cerr << "\nVektorfehler: " << str << '\n' << std::endl;
	exit(1);
}
