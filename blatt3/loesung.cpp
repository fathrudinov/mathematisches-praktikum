#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#include "unit.h"

#include "matrix.h"
#include "vector.h"

std::tuple<mapra::Vector, double, std::int64_t> CalculateEigenvector(
		const mapra::Matrix& A, const mapra::Vector& x0, double eps) {
	std::size_t i = 0;
	std::size_t k = 0;

	mapra::Vector xs_last(0);
	mapra::Vector xs_cur(0);
	mapra::Vector x_last(0);
	mapra::Vector x_cur(0);

	double eigenwert;
	x_cur = x0;

	do {
		xs_last = xs_cur;
		x_last = x_cur;
		i++;

		xs_cur = A * x_last;
		eigenwert = xs_cur(k);

		if (2 * std::abs(xs_cur(k)) <= xs_cur.NormMax()){
			std::size_t maxindex = 0;
			for (std::size_t i = 0; i < x_cur.GetLength(); i++) {
				if (std::abs(xs_cur(maxindex)) <= std::abs(xs_cur(i)))
					maxindex = i;
			}
			k = maxindex;
		}

		x_cur = xs_cur / xs_cur(k);
	} while ((x_cur-x_last).NormMax() > eps || std::abs(xs_cur(k) - xs_last(k)) > eps);

	return std::make_tuple(x_cur, eigenwert, i);
}

int main(int argc, char** argv) {
	if (argc <= 1) {
		std::cerr << "Du musst einen Parameter angeben: " << argv[0] << " <Beispielnr.>" << std::endl;
		return 1;
	}

	std::istringstream stream(argv[1]);
	int input;
	stream >> input;

	if (stream.fail()) {
		std::cerr << "Gebe ein Integer für die Beispielnummer ein: " << argv[0] << " <Beispielnr.>" << std::endl;
		return 1;
	}

	auto [A, x, eps] = mapra::GetExample(input);
	auto [eig_vec, eig_val, iterations] = CalculateEigenvector(A, x, eps);

	CheckSolution(eig_vec, eig_val, iterations);

	return 0;
}
