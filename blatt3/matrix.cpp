
#include "matrix.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>
#include <iomanip>

// =======================
//      Konstruktoren
// =======================

// ----- Konstruktor -----

mapra::Matrix::Matrix(std::size_t r, std::size_t c) : rows_(r), cols_(c), elems_(r * c, 0) {}

mapra::Matrix::Matrix(const mapra::Matrix& matrix) : rows_(matrix.rows_), cols_(matrix.cols_), elems_(matrix.rows_ * matrix.cols_, 0) {
	(*this) = matrix;
}

// ===========================================
//      Matrixelement schreiben und lesen
// ===========================================

// ----- Schreib- und Lesezugriff auf Matrixelemente -----

double& mapra::Matrix::operator()(std::size_t r, std::size_t c) {
#ifndef NDEBUG
	if (r >= rows_ || c >= cols_) {
		mapra::Matrix::MatError("Ungültiger Index!");
	}
#endif

	return elems_[r * cols_ + c];
}

// ----- Lesezugriff auf Elemente konstanter Matrizen -----

double mapra::Matrix::operator()(std::size_t r, std::size_t c) const {
#ifndef NDEBUG
	if (r >= rows_ || c >= cols_) {
		mapra::Matrix::MatError("Ungültiger Index!");
	}
#endif

	return elems_[r * cols_ + c];
}

// =====================
//      Zuweisungen
// =====================

// ----- Zuweisungsoperator "=" ----

mapra::Matrix& mapra::Matrix::operator=(const mapra::Matrix& other) {
	if (rows_ != other.rows_ || cols_ != other.cols_)
		Redim(other.rows_, other.cols_);

	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < cols_; c++) {
			(*this)(r, c) = other(r, c);
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Addition "+=" ----

mapra::Matrix& mapra::Matrix::operator+=(const mapra::Matrix& other) {
#ifndef NDEBUG
	if (rows_ != other.rows_ || cols_ != other.cols_) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < other.cols_; c++) {
			(*this)(r, c) += other(r, c);
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Subtraktion "-=" ----

mapra::Matrix& mapra::Matrix::operator-=(const mapra::Matrix& other) {
#ifndef NDEBUG
	if (rows_ != other.rows_ || cols_ != other.cols_) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < other.cols_; c++) {
			(*this)(r, c) -= other(r, c);
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Multiplikation "*=" ----

mapra::Matrix& mapra::Matrix::operator*=(const mapra::Matrix& other) {
#ifndef NDEBUG
	if (cols_ != other.rows_) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix result(rows_, other.cols_);

	// Matrixmultiplikation durchführen
	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < other.cols_; c++) {
			for (std::size_t i = 0; i < cols_; i++) {
				result(r, c) += (*this)(r, i) * other(i, c);
			}
		}
	}

	// Kopieren, statt: elems_ = result.elems_;
	std::size_t rows = rows_, cols = other.cols_;
	Redim(rows, cols);
	for (std::size_t r = 0; r < rows; r++) {
		for (std::size_t c = 0; c < cols; c++) {
			(*this)(r, c) = result(r, c);
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Multiplikation "*=" ----

mapra::Matrix& mapra::Matrix::operator*=(double a) {
	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < cols_; c++) {
			(*this)(r, c) *= a;
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Division "/=" ----

mapra::Matrix& mapra::Matrix::operator/=(double a) {
	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < cols_; c++) {
			(*this)(r, c) /= a;
		}
	}

	return *this;
}

// ==============================
//      Matrixlaenge aendern
// ==============================

// ----- Matrixlaenge aendern -----

mapra::Matrix& mapra::Matrix::Redim(std::size_t r, std::size_t c) {
	rows_ = r;
	cols_ = c;
	elems_ = std::vector<double>(r * c, 0);

	return *this;
}

std::size_t mapra::Matrix::GetRows() const { return rows_; }
std::size_t mapra::Matrix::GetCols() const { return cols_; }

// ==================================
//      arithmetische Operatoren
// ==================================

// ----- Addition "+" -----

mapra::Matrix mapra::operator+(const mapra::Matrix& x, const mapra::Matrix& y) {
#ifndef NDEBUG
	if (x.rows_ != y.rows_ || x.cols_ != y.cols_) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix result = x;
	return result += y;
}

// ----- Subtraktion "-" -----

mapra::Matrix mapra::operator-(const mapra::Matrix& x, const mapra::Matrix& y) {
#ifndef NDEBUG
	if (x.rows_ != y.rows_ || x.cols_ != y.cols_) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix result = x;
	return result -= y;
}

// ----- Vorzeichen wechseln "-" -----

mapra::Matrix mapra::operator-(const mapra::Matrix& x) {
	mapra::Matrix result(x.rows_, x.cols_);
	return result -= x;
}

// ----- Matrix-Multiplikation Matrix*Matrix "*" -----

mapra::Matrix mapra::operator*(const mapra::Matrix& x, const mapra::Matrix& y) {
#ifndef NDEBUG
	if (x.cols_ != y.rows_) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix result = x;
	return result *= y;
}

// ----- Multiplikation Skalar*Matrix "*" -----

mapra::Matrix mapra::operator*(double c, const mapra::Matrix& x) {
	mapra::Matrix result = x;
	return result *= c;
}

// ----- Multiplikation Matrix*Skalar "*" -----

mapra::Matrix mapra::operator*(const mapra::Matrix& x, double c) {
	mapra::Matrix result = x;
	return result *= c;
}

// ----- Division Matrix/Skalar "/" -----

mapra::Matrix mapra::operator/(const mapra::Matrix& x, double c) {
	mapra::Matrix result = x;
	return result /= c;
}

// ==============================
//      Vergleichsoperatoren
// ==============================

// ----- Test auf Gleichheit "==" -----

bool mapra::operator==(const mapra::Matrix& x, const mapra::Matrix& y) {
	if (x.rows_ != y.rows_)
		return false;
	if (x.cols_ != y.cols_)
		return false;

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t c = 0; c < x.cols_; c++) {
			if (x(r, c) != y(r, c))
				return false;
		}
	}

	return true;
}

// ----- Test auf Ungleichheit "!=" -----

bool mapra::operator!=(const mapra::Matrix& x, const mapra::Matrix& y) {
	return !(x == y);
}

// ==============================
//      Matrix-Vektor Produkte
// ==============================

// ----- Multiplikation Matrix*Vektor -----

mapra::Vector mapra::operator*(const mapra::Matrix& x, const mapra::Vector& y) {
#ifndef NDEBUG
	if (x.cols_ != y.GetLength()) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Vector result(x.rows_);

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t i = 0; i < x.cols_; i++) {
			result(r) += x(r, i) * y(i);
		}
	}

	return result;
}

// ----- Multiplikation Vektor*Matrix -----

mapra::Vector mapra::operator*(const mapra::Vector& x, const mapra::Matrix& y) {
#ifndef NDEBUG
	if (x.GetLength() != y.rows_) {
		mapra::Matrix::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Vector result(y.cols_);

	for (std::size_t c = 0; c < y.cols_; c++) {
		for (std::size_t i = 0; i < y.rows_; i++) {
			result(c) += x(i) * y(i, c);
		}
	}

	return result;
}

// ==========================
//      Ein- und Ausgabe
// ==========================

// ----- Ausgabe "<<" -----

std::ostream& mapra::operator<<(std::ostream& s, const mapra::Matrix& x) {
	s << std::setiosflags(std::ios::right);

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t c = 0; c < x.cols_; c++) {
			s << "\n(" << std::setw(4) << r << ", " << std::setw(4) << c << ") " << x(r, c);
		}
	}

	return s << std::endl;
}

// ----- Eingabe ">>" -----

std::istream& mapra::operator>>(std::istream& s, mapra::Matrix& x) {
	std::cout << std::setiosflags(std::ios::right);

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t c = 0; c < x.cols_; c++) {
			std::cout << "\n(" << std::setw(4) << r << ", " << std::setw(4) << c << ") " << std::flush;
			s >> x(r, c);
		}
	}

	return s;
}

// ==========================
//      Fehlerbehandlung
// ==========================

// ----- Ausgabe der Fehlermeldung "str" -----

void mapra::Matrix::MatError(const char str[]) {
	std::cerr << "\nMatrixfehler: " << str << '\n' << std::endl;
	exit(1);
}
