#include <algorithm>
#include <iostream>
#include <string>

#include "mapra_test.h"
#include "unit.h"

#include "sorting.h"

void testsort(
		mapra::MapraTest& tester,
		const char* algname,
		std::vector<unsigned int> (*sortingalg)(const std::vector<unsigned int>&),
		int length) {
	for (int i = 0; i <= 4; i++) {
		const std::string message = std::string(algname) + " mit Test Set #" + std::to_string(i);
		std::cout << "\nTeste " << message << " ..." << std::endl;

		std::vector<unsigned int> array = mapra::GetExample(i, length);
		std::vector<unsigned int> sorted = sortingalg(array);
		bool result = mapra::CheckSolution(sorted);

		tester.Assert(message, result);
	}
}

int main() {
	mapra::MapraTest tester("Tester für Sortieralgorithmen");

	int length = 20;

	testsort(tester, "Bubble Sort",     sorting::bubblesort,     length);
	testsort(tester, "Selection Sort",  sorting::selectionsort,  length);
	testsort(tester, "Insertion Sort",  sorting::insertionsort,  length);
	testsort(tester, "Heap Sort",       sorting::heapsort,       length);
	testsort(tester, "Quick Sort",      sorting::quicksort,      length);
	testsort(tester, "Quick Sort MF3",  sorting::quicksort_mv3,  length);
	testsort(tester, "Merge Sort",      sorting::mergesort,      length);

	return 0;
}
