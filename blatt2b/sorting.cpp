#include "sorting.h"

#include <algorithm>
#include <iostream>
#include <string>

namespace sorting {

std::vector<unsigned int> bubblesort(const std::vector<unsigned int>& array){
	std::vector<unsigned int> result = std::vector<unsigned int>(array);

	int length = array.size();
	for (int i = 0; i < length - 1; i++) {
		for (int j = 0; j < length - i - 1; j++) {
			if (result[j] > result[j+1])
				std::swap(result[j], result[j+1]);
		}
	}

	return result;
}
std::vector<unsigned int> selectionsort(const std::vector<unsigned int>& array) {
	std::vector<unsigned int> result = std::vector<unsigned int>(array);

	for (int start = 0; start < array.size(); start++) {
		int toswap = start;
		for (int i = 0; i < start; i++) {
			if (result[toswap] < result[i])
				std::swap(result[toswap], result[i]);
		}
	}

	return result;
}

std::vector<unsigned int> insertionsort(const std::vector<unsigned int>& array) {
	std::vector<unsigned int> result = std::vector<unsigned int>(array);

	for (int start = 0; start < array.size(); start++) {
		int min = start;
		for (int i = start; i < array.size(); i++) {
			if (result[i] < result[min])
				min = i;
		}
		std::swap(result[start], result[min]);
	}

	return result;
}

void heapify(std::vector<unsigned int>& array, int last, int pos) {
	int max = pos;
	int left = 2 * pos + 1;
	int right = 2 * pos + 2;

	if (left < last && array[left] > array[max])
		max = left;

	if (right < last && array[right] > array[max])
		max = right;

	if (max != pos) {
		std::swap(array[pos], array[max]);
		heapify(array, last, max);
	}
}

std::vector<unsigned int> heapsort(const std::vector<unsigned int>& array) {
	std::vector<unsigned int> result = std::vector<unsigned int>(array);

	int length = result.size();

	for (int i = length / 2 - 1; i >= 0; i--)
		heapify(result, length, i);

	for (int i = length - 1; i > 0; i--) {
		std::swap(result[0], result[i]);
		heapify(result, i, 0);
	}

	return result;
}

void quicksortpart(std::vector<unsigned int>& array, int start, int end, bool optimize_pivot) {
	if (end <= start)
		return;

	int left = start;
	int right = end - 1;
	int pivot = array[end];

	// "Median von Drei"
	if (optimize_pivot) {
		int middle = (start + end) / 2;

		bool lm = array[start]  < array[middle];
		bool mr = array[middle] < array[end];
		bool lr = array[start]  < array[end];

		/*
		 lm,  mr,  lr  =>  middle
		 lm,  mr, !lr  =>  --
		 lm, !mr,  lr  =>  end
		 lm, !mr, !lr  =>  start
		!lm,  mr,  lr  =>  start
		!lm,  mr, !lr  =>  end
		!lm, !mr,  lr  =>  --
		!lm, !mr, !lr  =>  middle
		*/

		// siehe tabelle oben, das ist der fall wo nicht unmöglich (--) oder end (schon oben gesetzt)
		if (mr == lr) {
			int pivotindex = (lm == mr) ? middle : start;
			std::swap(array[pivotindex], array[end]);
			pivot = array[end];
		}
	}

	while (left < right) {
		while (left < end - 1 && array[left] <= pivot)
			left++;

		while (right > start && array[right] >= pivot)
			right--;

		if (left >= right)
			break;

		std::swap(array[left], array[right]);
	}

	// left und right zeigen immer auf das was als nächstes GEPRÜFT WERDEN SOLL, nicht geprüft ist.
	// left und right sind bei diesem punkt auf dem gleichen mittleren element was noch nicht gechekt wurde
	// wenn es kleiner als pivot ist, sollte pivot also mit dem rechts daneben getauscht werden!
	int pivotindex = (array[left] < array[end]) ? (left + 1) : left;

	if (end != pivotindex)
		std::swap(array[end], array[pivotindex]);

	quicksortpart(array, start, pivotindex - 1, optimize_pivot);
	quicksortpart(array, pivotindex + 1, end,   optimize_pivot);
}

std::vector<unsigned int> quicksort(const std::vector<unsigned int>& array) {
	std::vector<unsigned int> result = std::vector<unsigned int>(array);

	quicksortpart(result, 0, result.size() - 1, false);

	return result;
}

std::vector<unsigned int> quicksort_mv3(const std::vector<unsigned int>& array) {
	std::vector<unsigned int> result = std::vector<unsigned int>(array);

	quicksortpart(result, 0, result.size() - 1, true);

	return result;
}

std::vector<unsigned int> merge(std::vector<unsigned int>& array, int links, int mitte, int rechts){
	int leftlength = mitte - links + 1;
	int rightlength = rechts - mitte;

	std::vector<unsigned int> L;
	L.resize(leftlength);

	std::vector<unsigned int> M;
	M.resize(rightlength);

	for(int i = 0; i < leftlength; i++) {
		L[i] = array[links + i];
	}

	for(int i = 0; i < rightlength; i++) {
		M[i] = array[mitte + 1 + i];
	}

	int i = 0;
	int j = 0;
	int k = links;

	while (i < leftlength && j < rightlength) {
		if (L[i] <= M[j]){
			array[k] = L[i];
			i++;
		} else {
			array[k] = M[j];
			j++;
		}
		k++;
	}

	while (i < leftlength) {
		array[k] = L[i];
		i++;
		k++;
	}

	while (j < rightlength) {
		array[k] = M[j];
		j++;
		k++;
	}

	return array;
}

std::vector<unsigned int> mergesortpart(std::vector<unsigned int>& array, int links, int rechts){
	if (links < rechts) {
		int mitte = (links + rechts) / 2;
		array = mergesortpart(array, links, mitte);
		array = mergesortpart(array, mitte + 1, rechts);
		array = merge(array, links, mitte, rechts);
	}
	return array;
}

std::vector<unsigned int> mergesort(const std::vector<unsigned int>& array){
	std::vector<unsigned int> result = std::vector<unsigned int>(array);

	result = mergesortpart(result, 0, result.size() - 1);

	return result;
}

}  // namespace sorting
