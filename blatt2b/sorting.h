#ifndef SORTING_H_
#define SORTING_H_

#include <iostream>
#include <utility>
#include <vector>

namespace sorting {

std::vector<unsigned int> bubblesort(const std::vector<unsigned int>& array);

std::vector<unsigned int> selectionsort(const std::vector<unsigned int>& array);

std::vector<unsigned int> insertionsort(const std::vector<unsigned int>& array);

//void heapify(std::vector<unsigned int>& array, int length, int i);

std::vector<unsigned int> heapsort(const std::vector<unsigned int>& array);

//void quicksortpart(std::vector<unsigned int>& array, int start, int end, bool optimize_pivot) {

std::vector<unsigned int> quicksort(const std::vector<unsigned int>& array);

std::vector<unsigned int> quicksort_mv3(const std::vector<unsigned int>& array);

//std::vector<unsigned int> merge(std::vector<unsigned int>& array, int links, int mitte, int rechts){

//std::vector<unsigned int> mergesortpart(std::vector<unsigned int>& array, int links, int rechts){

std::vector<unsigned int> mergesort(const std::vector<unsigned int>& array);

}  // namespace sorting

#endif  // SORTING_H_
