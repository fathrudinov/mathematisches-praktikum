#ifndef SORTING_H_
#define SORTING_H_

#include <iostream>
#include <utility>
#include <vector>

namespace sorting {

template<typename T>
void bubblesort(std::vector<T>& array) {
	unsigned int length = array.size();
	for (unsigned int i = 0; i < length - 1; i++) {
		for (unsigned int j = 0; j < length - i - 1; j++) {
			if (array[j] > array[j+1])
				std::swap(array[j], array[j+1]);
		}
	}
}

template<typename T>
void selectionsort(std::vector<T>& array) {
	for (unsigned int start = 0; start < array.size(); start++) {
		int toswap = start;
		for (unsigned int i = 0; i < start; i++) {
			if (array[toswap] < array[i])
				std::swap(array[toswap], array[i]);
		}
	}
}

template<typename T>
void merge(std::vector<T>& array, int links, int mitte, int rechts){
	int leftlength = mitte - links + 1;
	int rightlength = rechts - mitte;

	std::vector<T> L;
	L.resize(leftlength);

	std::vector<T> M;
	M.resize(rightlength);

	for(int i = 0; i < leftlength; i++) {
		L[i] = array[links + i];
	}

	for(int i = 0; i < rightlength; i++) {
		M[i] = array[mitte + 1 + i];
	}

	int i = 0;
	int j = 0;
	int k = links;

	while (i < leftlength && j < rightlength) {
		if (L[i] <= M[j]){
			array[k] = L[i];
			i++;
		} else {
			array[k] = M[j];
			j++;
		}
		k++;
	}

	while (i < leftlength) {
		array[k] = L[i];
		i++;
		k++;
	}

	while (j < rightlength) {
		array[k] = M[j];
		j++;
		k++;
	}
}

template<typename T>
void mergesortpart(std::vector<T>& array, int links, int rechts) {
	if (links < rechts) {
		int mitte = (links + rechts) / 2;
		mergesortpart(array, links, mitte);
		mergesortpart(array, mitte + 1, rechts);
		merge(array, links, mitte, rechts);
	}
}

template<typename T>
void mergesort(std::vector<T>& array) {
	mergesortpart(array, 0, array.size() - 1);
}

}  // namespace sorting

#endif  // SORTING_H_
