#ifndef FILEIO_H_
#define FILEIO_H_

#include <iostream>
#include <vector>

namespace mapra {

template<typename T>
void Read(std::ifstream& ifs, std::vector<T>& array) {
	while (!ifs.eof() && !ifs.fail()) {
		T input;
		ifs >> input;

		if (!ifs.fail())
			array.push_back(input);
	}
}

template<typename T>
void Print(std::ostream& os, const std::vector<T>& array) {
	for (T output : array) {
		os << output;
	}
}

}  // namespace mapra

#endif  // STUDENT_H_
