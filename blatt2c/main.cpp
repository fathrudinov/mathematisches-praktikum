#include <iostream>
#include <fstream>
#include <string>

#include "unit.h"

#include "sorting.h"
#include "fileio.h"
#include "student.h"

template<typename T>
bool sort(const char* filename, int sortalg) {
	std::ifstream file(filename);
	std::vector<T> array;
	mapra::Read(file, array);
	file.close();

	if (sortalg == 0)
		sorting::bubblesort(array);
	else if (sortalg == 1)
		sorting::selectionsort(array);
	else if (sortalg == 2)
		sorting::mergesort(array);
	else
		return false;

	bool result = mapra::CheckSolution(array);
	return result;
}

int main() {
	std::cout << "Gebe den Algorithmus ein [B/A/M]: ";
	std::string input;
	std::cin >> input;

	int sortalg = -1;

	if (input == "B") {
		sortalg = 0;
	} else if (input == "A") {
		sortalg = 1;
	} else if (input == "M") {
		sortalg = 2;
	} else {
		std::cout << "Falsche Eingabe.\n";
		return 1;
	}

	if (!sort<double>("doubles.txt", sortalg))
		std::cout << "Doubles Array ist fehlgeschlagen." << std::endl;

	if (!sort<std::string>("strings.txt", sortalg))
		std::cout << "String Array ist fehlgeschlagen." << std::endl;

	if (!sort<mapra::Student>("studenten.txt", sortalg))
		std::cout << "Studenten Array ist fehlgeschlagen." << std::endl;

	return 0;
}
