#include <iostream>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>

#include "mapra_test.h"
#include "unit.h"

#include "sorting.h"
#include "fileio.h"
#include "student.h"

void TestSorting() {
	mapra::MapraTest test("Sorting Tester");

	mapra::Student malte  { "Malte",  "Schrödl",  398000, 1.0 };
	mapra::Student lasse  { "Lasse",  "Juretzko", 436269, 1.0 };
	mapra::Student kilian { "Kilian", "Kilian",   123456, 1.0 };
	mapra::Student sven   { "Sven",   "Gross",    654321, 1.0 };

	test.Assert("Sven vor Kilian", sven < kilian);
	test.Assert("Sven ungleich Kilian", sven != kilian);
	test.Assert("Lasse kleiner-gleich Malte", lasse <= malte);
	test.AssertEq("Malte gleich Malte", malte, malte);

	std::vector<mapra::Student> vector;
	vector.push_back(malte);
	vector.push_back(lasse);
	vector.push_back(kilian);
	vector.push_back(sven);

	sorting::mergesort(vector);

	test.AssertEq("Sortierte Liste", vector[0], sven);
	test.AssertEq("Sortierte Liste", vector[1], lasse);
	test.AssertEq("Sortierte Liste", vector[2], kilian);
	test.AssertEq("Sortierte Liste", vector[3], malte);
}

int main() {
	TestSorting();
	return 0;
}
