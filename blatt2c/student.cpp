// Copyright (c) 2022, The MaPra Authors.

#include "student.h"

// Eingabeoperator ">>"
std::istream& mapra::operator>>(std::istream& s, mapra::Student& a) {
  s >> a.first_name >> a.last_name >> a.matr_nr >> a.grade;
  return s;
}

// Ausgabeoperator "<<"
std::ostream& mapra::operator<<(std::ostream& s, const mapra::Student& a) {
  // hier den Ausgabeoperator definieren
  
  s << a.first_name <<" "<< a.last_name <<" "<< a.matr_nr <<" "<< a.grade;
  return s;
}

// Vergleichsoperator "<"
bool mapra::operator<(const mapra::Student& a, const mapra::Student& b) {
  if(a.last_name<b.last_name){
  	return true;
  }else if (a.last_name>b.last_name) {
  	return false;
  }else{
	return a.first_name<b.first_name;
  }
  // hier fehlt was
}




// Vergleichsoperatoren "==" bzw. "!="
bool mapra::operator==(const mapra::Student& a, const mapra::Student& b) {
   return (a.first_name == b.first_name && a.last_name == b.last_name );
}

bool mapra::operator!=(const mapra::Student& a, const mapra::Student& b) {
   return ! (a==b);
}

bool mapra::operator>(const mapra::Student& a, const mapra::Student& b) {
   if (a == b){
	   return false;
   }else{
	   return (!(a<b));
   }
}

// Vergleichsoperator "<"
bool mapra::operator<=(const mapra::Student& a, const mapra::Student& b) {
  return  (a<b || a==b);
}

