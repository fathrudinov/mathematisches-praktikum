
#include <iostream>

#include "mapra/matrix.h"
#include "mapra/vector.h"
#include "mapra/unit.h"

#include "loesung.h"

void RkStep(const VFFunction& f, real& t, RealVector& y, real& h) {
	std::size_t m = mapra::rk_alpha.GetLength();

	real epsilon;
	const real epsilon_max = mapra::eps;

	size_t i = 0;

	RealVector toadd(y.GetLength());

	real h_prev;
	do {
		h_prev = h;

		std::vector<RealVector> k(m);

		for (std::size_t j = 0; j < m; j++) {
			RealVector input = y;

			for (std::size_t l = 0; l < j; l++)
				input += h * mapra::rk_beta(l, j) * k[l];

			k[j] = f(t + mapra::rk_alpha(j) * h, input);
		}

		toadd = RealVector(y.GetLength());

		for (std::size_t l = 0; l < m; l++)
			toadd += h * mapra::rk_gamma(l) * k[l];

		// Schrittweiten

		RealVector e(y.GetLength());

		for (std::size_t l = 0; l < m; l++)
			e += h * mapra::rk_delta(l) * k[l];

		epsilon = e.NormMax();

		real c = 0.9 * std::pow(epsilon_max / epsilon, (real)1. / ((real)mapra::rk_p + 1.));
		h *= std::clamp(c, (real)0.1, (real)5.);
	} while (epsilon > epsilon_max && i < 3);

	y += toadd;
	t += h_prev;
}

RealVector gravityfunction(RealVector mass, const RealVector& y) {
	std::size_t n = mass.GetLength();
	std::size_t dim = y.GetLength() / (2 * n);

	std::vector<RealVector> xss(n);

	for (std::size_t i = 0; i < n; i++) {
		xss[i] = RealVector(dim);

		RealVector yi(dim);
		for (std::size_t k = 0; k < dim; k++)
			yi(k) = y(i * dim + k);

		for (std::size_t j = 0; j < n; j++) {
			if (i == j) continue;

			RealVector yj(dim);
			for (std::size_t k = 0; k < dim; k++)
				yj(k) = y(j * dim + k);

			auto massdiff = yj - yi;
			auto massdiffnorm = massdiff.Norm2();
			auto massdiffnormcubed = massdiffnorm * massdiffnorm * massdiffnorm;

			xss[i] += mapra::kGrav * mass(j) / massdiffnormcubed * massdiff;
		}
	}

	std::size_t middle = y.GetLength() / 2;
	RealVector result(y.GetLength());

	for (std::size_t i = 0; i < middle; i++)
		result(i) = y(i + middle);

	for (std::size_t i = middle; i < y.GetLength(); i++)
		result(i) = xss[(i - middle) / dim]((i - middle) % dim);

	return result;
}

int main(int argc, char** argv) {
	bool drawImage = true;

	if (argc <= 1) {
		std::cerr << "Please tell me what example to do" << std::endl;
		return 1;
	}

	if (argc > 2) {
		std::istringstream stream(argv[2]);
		stream >> drawImage;
		if (stream.fail()) {
			std::cerr << "Unbekannter zweiter Parameter. Nutze 1 (true) oder 0 (false), ob eine grafische Ausgabe erfolgen soll." << std::endl;
			return 1;
		}
	}

	std::istringstream stream(argv[1]);
	int examplenumber;
	stream >> examplenumber;

	std::cout << "Erstelle Example ..." << std::endl;
	auto [mass, fun, y_0, t_begin, t_end, h_0] = mapra::GetExample(examplenumber, true, true);

	if (mass.NormMax() == 0.) {
		std::cout << "Mass ist Nullvektor, Nutze vorgegebene Funktion." << std::endl;
	} else {
		std::cout << "Mass ist nicht Nullvektor, Nutze eigene Funktion für Mehrkörperproblem." << std::endl;
		fun = [=] (real, const RealVector& y) {
			return gravityfunction(mass, y);
		};
	}

	real t = t_begin;
	RealVector y = y_0;
	real h = h_0;

	std::cout << "Checke initialen step ..." << std::endl;
	CheckStep(t, y, drawImage, false);

	while (t < t_end) {
		if (t + h > t_end) {
			std::cout << "Korrektur angewendet." << std::endl;
			h = t_end - t;
		}

		RkStep(fun, t, y, h);
		std::cout << "Checking step t = " << t << " von " << t_end << " ..." << std::endl;
		CheckStep(t, y, drawImage, false);
	}

	std::cout << "Checking solution ..." << std::endl;
	CheckSolution(t, y);

	std::cout << "Sleeping." << std::endl;
	while (true);

	return 0;
}
