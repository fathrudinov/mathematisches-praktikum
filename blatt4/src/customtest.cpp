
#include <iostream>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>

#include "mapra/mapra_test.h"

#include "mapra/matrix.h"
#include "mapra/vector.h"

void TestMatrixVector() {
	mapra::MapraTest test("Matrix Tester");

	mapra::Matrix<unsigned long long> matrix1(8, 9);
	mapra::Matrix<unsigned long long> matrix2(8, 9);
	mapra::Vector<unsigned long long> vector1(9);
	mapra::Vector<unsigned long long> vector2(8);

	matrix1(1, 2) = 2ull;
	matrix2(1, 2) = 2ull;

	vector1(0) = 3ull;
	vector1(1) = 2ull;
	vector1(2) = 1ull;

	vector2(1) = 2ull;

	test.AssertEq("Matrizen gleich", matrix1, matrix2);
	test.AssertEq("Matrix-Vektor-Produkt", matrix1 * vector1, vector2);

	matrix1.Redim(2, 2);
	matrix2.Redim(2, 2);
	test.AssertEq("Redim Rows", matrix1.GetRows(), (std::size_t)2);
	test.AssertEq("Redim Cols", matrix1.GetCols(), (std::size_t)2);

	test.AssertEq("Matrix-Matrix-Produkt", matrix1 * matrix2, matrix1);

	test.AssertEq("Zugriff Matrix", matrix1(1, 1), 0ull);
	test.AssertEq("Zugriff Vektor", vector1(2),    1ull);
}

int main() {
	TestMatrixVector();
	return 0;
}
