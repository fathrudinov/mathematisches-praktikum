
#include <algorithm>

#include "gtest/gtest.h"

#include "mapra/vector.h"
#include "mapra/matrix.h"

TEST(VectorTest, VectorSize) {
	mapra::Vector<long> v(7);
	EXPECT_EQ(v.GetLength(), 7);
}

TEST(VectorTest, VectorAdd) {
	mapra::Vector<int> v(3);
	v(0) = 9;
	v(1) = -3;
	v(2) = 17;
	mapra::Vector<int> w(3);
	w(0) = 1;
	w(1) = 3;
	w(2) = 7;
	mapra::Vector<int> u(3);
	u(0) = 10;
	u(1) = 0;
	u(2) = 24;

	EXPECT_EQ(v + w, u);
}

TEST(VectorTest, VectorCopy) {
	mapra::Vector<int> v(3);
	v(0) = 9;
	v(1) = -3;
	v(2) = 17;
	mapra::Vector<int> w = v;

	v(1) = 1;

	mapra::Vector<int> u(3);
	u(0) = 9;
	u(1) = -3;
	u(2) = 17;

	EXPECT_NE(v, w);
	EXPECT_EQ(w, u);
}

TEST(VectorTest, VectorOperators) {
	mapra::Vector<int> v(3);
	v(0) = 9;
	v(1) = -3;
	v(2) = 17;
	mapra::Vector<int> w(3);
	w(0) = 1;
	w(1) = 3;
	w(2) = 7;
	mapra::Vector<int> u(3);
	u(0) = 10;
	u(1) = 0;
	u(2) = 24;

	mapra::Vector<int> u_orig = u;

	v += w;
	EXPECT_EQ(v, u);

	u -= w;
	v -= w;
	EXPECT_EQ(v, u);

	v += w;
	EXPECT_EQ(v, u_orig);

	EXPECT_NE(u, u_orig);
}

TEST(VectorTest, MatrixMultiplication) {
	mapra::Matrix<int> mat(3, 3);
	mat(0, 1) = 4;
	mat(0, 2) = 5;
	mat(2, 2) = 1;

	mapra::Vector<int> v(3);
	v(0) = 9;
	v(1) = -3;
	v(2) = 17;
	mapra::Vector<int> w(3);
	w(0) = 4 * (-3) + 5 * 17;
	w(1) = 0;
	w(2) = 1 * 17;

	EXPECT_EQ(mat * v, w);
}

TEST(VectorTest, MaxNorm) {
	mapra::Vector<int> v(3);
	v(0) = 9;
	v(1) = -3;
	v(2) = 17;
	mapra::Vector<int> w(3);
	w(0) = 1;
	w(1) = 3;
	w(2) = 7;
	mapra::Vector<int> u(3);
	u(0) = 10;
	u(1) = 23;
	u(2) = -24;

	EXPECT_EQ(v.NormMax(), 17);
	EXPECT_EQ(w.NormMax(), 7);
	EXPECT_EQ(u.NormMax(), 24);
}

#define FAILTEST 0
#if FAILTEST
TEST(CustomTest, Fail) {
	EXPECT_NE(1, 1);
}
#endif
