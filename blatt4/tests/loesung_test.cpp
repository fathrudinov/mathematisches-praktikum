
#include <algorithm>

#include "gtest/gtest.h"

#include "mapra/vector.h"
#include "mapra/matrix.h"
#include "loesung.h"

#if 0
TEST(LoesungTest, RkStepTest) {
	const VFFunction fun = [=] (real t, const RealVector&) {
		RealVector result(1);
		result(0) = 3.0 * t * t;
		return result;
	};

	real t = 0.;
	real h = .2;
	real y0 = 1.;

	RealVector y(1);
	y(0) = y0;

	RkStep(fun, t, y, h);

	EXPECT_EQ(y(0), y0 + h * h * h);
}
#endif
