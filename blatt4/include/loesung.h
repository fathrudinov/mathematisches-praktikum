
#include "mapra/matrix.h"
#include "mapra/vector.h"
#include "mapra/unit.h"

void RkStep(const VFFunction& f, real& t, RealVector& y, real& h);

RealVector gravityfunction(RealVector mass, const RealVector& y);

int main(int argc, char** argv);
