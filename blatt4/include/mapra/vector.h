// Copyright (c) 2022, The MaPra Authors.

#ifndef VECTOR_H_
#define VECTOR_H_

#include <cstddef>
#include <iostream>
#include <vector>

namespace mapra {

template <typename T = double>
class Vector {
 public:
  explicit Vector(std::size_t len = 1);
  Vector(const Vector<T>& vector);

  T& operator()(std::size_t);
  T operator()(std::size_t) const;

  Vector<T>& operator=(const Vector<T>&);
  Vector<T>& operator+=(const Vector<T>&);
  Vector<T>& operator-=(const Vector<T>&);
  Vector<T>& operator*=(T);
  Vector<T>& operator/=(T);

  Vector<T>& Redim(std::size_t);
  std::size_t GetLength() const;
  T Norm2() const;
  T NormMax() const;

  static void VecError(const char str[]);

  template <typename U> friend Vector<U> operator+(const Vector<U>&, const Vector<U>&);
  template <typename U> friend Vector<U> operator-(const Vector<U>&, const Vector<U>&);
  template <typename U> friend Vector<U> operator-(const Vector<U>&);

  template <typename U> friend U operator*(const Vector<U>&, const Vector<U>&);
  template <typename U> friend Vector<U> operator*(U, const Vector<U>&);
  template <typename U> friend Vector<U> operator*(const Vector<U>&, U);
  template <typename U> friend Vector<U> operator/(const Vector<U>&, U);

  template <typename U> friend bool operator==(const Vector<U>&, const Vector<U>&);
  template <typename U> friend bool operator!=(const Vector<U>&, const Vector<U>&);

  template <typename U> friend std::istream& operator>>(std::istream&, Vector<U>&);
  template <typename U> friend std::ostream& operator<<(std::ostream&, const Vector<U>&);

 private:
  std::vector<T> elems_;
};

template <typename T> Vector<T> operator+(const Vector<T>&, const Vector<T>&);
template <typename T> Vector<T> operator-(const Vector<T>&, const Vector<T>&);
template <typename T> Vector<T> operator-(const Vector<T>&);

template <typename T> T operator*(const Vector<T>&, const Vector<T>&);
template <typename T> Vector<T> operator*(T, const Vector<T>&);
template <typename T> Vector<T> operator*(const Vector<T>&, T);
template <typename T> Vector<T> operator/(const Vector<T>&, T);

template <typename T> bool operator==(const Vector<T>&, const Vector<T>&);
template <typename T> bool operator!=(const Vector<T>&, const Vector<T>&);

template <typename T> std::istream& operator>>(std::istream&, Vector<T>&);
template <typename T> std::ostream& operator<<(std::ostream&, const Vector<T>&);

} // namespace mapra

#include "vector-impl.h"

#endif  // VECTOR_H_
