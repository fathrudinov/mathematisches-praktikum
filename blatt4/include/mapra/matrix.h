// Copyright (c) 2022, The MaPra Authors.

#ifndef MATRIX_H_
#define MATRIX_H_

#include <cmath>
#include <cstddef>
#include <iostream>
#include <vector>

#include "mapra/vector.h"

namespace mapra {

template <typename T = double>
class Matrix {
 public:
  explicit Matrix(std::size_t r = 1, std::size_t c = 1);

  T& operator()(std::size_t i, std::size_t j);
  T operator()(std::size_t i, std::size_t j) const;

  Matrix<T>& operator+=(const Matrix<T>&);
  Matrix<T>& operator-=(const Matrix<T>&);
  Matrix<T>& operator*=(const Matrix<T>&);
  Matrix<T>& operator*=(T);
  Matrix<T>& operator/=(T);

  Matrix<T>& Redim(std::size_t, std::size_t);
  std::size_t GetRows() const;
  std::size_t GetCols() const;

  static void MatError(const char str[]);

  template <typename U> friend Matrix<U> operator+(const Matrix<U>&, const Matrix<U>&);
  template <typename U> friend Matrix<U> operator-(const Matrix<U>&, const Matrix<U>&);
  template <typename U> friend Matrix<U> operator-(const Matrix<U>&);

  template <typename U> friend Matrix<U> operator*(const Matrix<U>&, const Matrix<U>&);
  template <typename U> friend Matrix<U> operator*(U, const Matrix<U>&);
  template <typename U> friend Matrix<U> operator*(const Matrix<U>&, U);
  template <typename U> friend Matrix<U> operator/(const Matrix<U>&, U);

  template <typename U> friend bool operator==(const Matrix<U>&, const Matrix<U>&);
  template <typename U> friend bool operator!=(const Matrix<U>&, const Matrix<U>&);

  template <typename U> friend std::istream& operator>>(std::istream&, Matrix<U>&);
  template <typename U> friend std::ostream& operator<<(std::ostream&, const Matrix<U>&);

  template <typename U> friend Vector<U> operator*(const Matrix<U>&, const Vector<U>&);
  template <typename U> friend Vector<U> operator*(const Vector<U>&, const Matrix<U>&);

 private:
  std::size_t rows_;
  std::size_t cols_;
  std::vector<T> elems_;
};

template <typename T> Matrix<T> operator+(const Matrix<T>&, const Matrix<T>&);
template <typename T> Matrix<T> operator-(const Matrix<T>&, const Matrix<T>&);
template <typename T> Matrix<T> operator-(const Matrix<T>&);

template <typename T> Matrix<T> operator*(const Matrix<T>&, const Matrix<T>&);
template <typename T> Matrix<T> operator*(T, const Matrix<T>&);
template <typename T> Matrix<T> operator*(const Matrix<T>&, T);
template <typename T> Matrix<T> operator/(const Matrix<T>&, T);

template <typename T> bool operator==(const Matrix<T>&, const Matrix<T>&);
template <typename T> bool operator!=(const Matrix<T>&, const Matrix<T>&);

template <typename T> std::istream& operator>>(std::istream&, Matrix<T>&);
template <typename T> std::ostream& operator<<(std::ostream&, const Matrix<T>&);

template <typename T> Vector<T> operator*(const Matrix<T>&, const Vector<T>&);
template <typename T> Vector<T> operator*(const Vector<T>&, const Matrix<T>&);

}  // namespace mapra

#include "matrix-impl.h"

#endif  // MATRIX_H_
