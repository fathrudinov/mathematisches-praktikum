
#include "matrix.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>
#include <iomanip>

// =======================
//      Konstruktoren
// =======================

// ----- Konstruktor -----

template <typename T>
mapra::Matrix<T>::Matrix(std::size_t r, std::size_t c) : rows_(r), cols_(c), elems_(r * c, 0) {}

/*
template <typename T>
mapra::Matrix<T>::Matrix(const mapra::Matrix<T>& matrix) : rows_(matrix.rows_), cols_(matrix.cols_), elems_(matrix.rows_ * matrix.cols_, 0) {
	(*this) = matrix;
}
*/

// ===========================================
//      Matrixelement schreiben und lesen
// ===========================================

// ----- Schreib- und Lesezugriff auf Matrixelemente -----

template <typename T>
T& mapra::Matrix<T>::operator()(std::size_t r, std::size_t c) {
#ifndef NDEBUG
	if (r >= rows_ || c >= cols_) {
		mapra::Matrix<T>::MatError("Ungültiger Index!");
	}
#endif

	return elems_[r * cols_ + c];
}

// ----- Lesezugriff auf Elemente konstanter Matrizen -----

template <typename T>
T mapra::Matrix<T>::operator()(std::size_t r, std::size_t c) const {
#ifndef NDEBUG
	if (r >= rows_ || c >= cols_) {
		mapra::Matrix<T>::MatError("Ungültiger Index!");
	}
#endif

	return elems_[r * cols_ + c];
}

// =====================
//      Zuweisungen
// =====================

// ----- Zuweisungsoperator "=" ----

/*
template <typename T>
mapra::Matrix<T>& mapra::Matrix<T>::operator=(const mapra::Matrix<T>& other) {
	if (rows_ != other.rows_ || cols_ != other.cols_)
		Redim(other.rows_, other.cols_);

	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < cols_; c++) {
			(*this)(r, c) = other(r, c);
		}
	}

	return *this;
}
*/

// ----- Zuweisungsoperator mit Addition "+=" ----

template <typename T>
mapra::Matrix<T>& mapra::Matrix<T>::operator+=(const mapra::Matrix<T>& other) {
#ifndef NDEBUG
	if (rows_ != other.rows_ || cols_ != other.cols_) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < other.cols_; c++) {
			(*this)(r, c) += other(r, c);
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Subtraktion "-=" ----

template <typename T>
mapra::Matrix<T>& mapra::Matrix<T>::operator-=(const mapra::Matrix<T>& other) {
#ifndef NDEBUG
	if (rows_ != other.rows_ || cols_ != other.cols_) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < other.cols_; c++) {
			(*this)(r, c) -= other(r, c);
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Multiplikation "*=" ----

template <typename T>
mapra::Matrix<T>& mapra::Matrix<T>::operator*=(const mapra::Matrix<T>& other) {
#ifndef NDEBUG
	if (cols_ != other.rows_) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix<T> result(rows_, other.cols_);

	// Matrixmultiplikation durchführen
	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < other.cols_; c++) {
			for (std::size_t i = 0; i < cols_; i++) {
				result(r, c) += (*this)(r, i) * other(i, c);
			}
		}
	}

	// Kopieren, statt: elems_ = result.elems_;
	std::size_t rows = rows_, cols = other.cols_;
	Redim(rows, cols);
	for (std::size_t r = 0; r < rows; r++) {
		for (std::size_t c = 0; c < cols; c++) {
			(*this)(r, c) = result(r, c);
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Multiplikation "*=" ----

template <typename T>
mapra::Matrix<T>& mapra::Matrix<T>::operator*=(T a) {
	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < cols_; c++) {
			(*this)(r, c) *= a;
		}
	}

	return *this;
}

// ----- Zuweisungsoperator mit Division "/=" ----

template <typename T>
mapra::Matrix<T>& mapra::Matrix<T>::operator/=(T a) {
	for (std::size_t r = 0; r < rows_; r++) {
		for (std::size_t c = 0; c < cols_; c++) {
			(*this)(r, c) /= a;
		}
	}

	return *this;
}

// ==============================
//      Matrixlaenge aendern
// ==============================

// ----- Matrixlaenge aendern -----

template <typename T>
mapra::Matrix<T>& mapra::Matrix<T>::Redim(std::size_t r, std::size_t c) {
	rows_ = r;
	cols_ = c;
	elems_ = std::vector<T>(r * c, 0);

	return *this;
}

template <typename T>
std::size_t mapra::Matrix<T>::GetRows() const { return rows_; }
template <typename T>
std::size_t mapra::Matrix<T>::GetCols() const { return cols_; }

// ==================================
//      arithmetische Operatoren
// ==================================

// ----- Addition "+" -----

template <typename T>
mapra::Matrix<T> mapra::operator+(const mapra::Matrix<T>& x, const mapra::Matrix<T>& y) {
#ifndef NDEBUG
	if (x.rows_ != y.rows_ || x.cols_ != y.cols_) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix<T> result = x;
	return result += y;
}

// ----- Subtraktion "-" -----

template <typename T>
mapra::Matrix<T> mapra::operator-(const mapra::Matrix<T>& x, const mapra::Matrix<T>& y) {
#ifndef NDEBUG
	if (x.rows_ != y.rows_ || x.cols_ != y.cols_) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix<T> result = x;
	return result -= y;
}

// ----- Vorzeichen wechseln "-" -----

template <typename T>
mapra::Matrix<T> mapra::operator-(const mapra::Matrix<T>& x) {
	mapra::Matrix<T> result(x.rows_, x.cols_);
	return result -= x;
}

// ----- Matrix-Multiplikation Matrix*Matrix "*" -----

template <typename T>
mapra::Matrix<T> mapra::operator*(const mapra::Matrix<T>& x, const mapra::Matrix<T>& y) {
#ifndef NDEBUG
	if (x.cols_ != y.rows_) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Matrix<T> result = x;
	return result *= y;
}

// ----- Multiplikation Skalar*Matrix "*" -----

template <typename T>
mapra::Matrix<T> mapra::operator*(T c, const mapra::Matrix<T>& x) {
	mapra::Matrix<T> result = x;
	return result *= c;
}

// ----- Multiplikation Matrix*Skalar "*" -----

template <typename T>
mapra::Matrix<T> mapra::operator*(const mapra::Matrix<T>& x, T c) {
	mapra::Matrix<T> result = x;
	return result *= c;
}

// ----- Division Matrix/Skalar "/" -----

template <typename T>
mapra::Matrix<T> mapra::operator/(const mapra::Matrix<T>& x, T c) {
	mapra::Matrix<T> result = x;
	return result /= c;
}

// ==============================
//      Vergleichsoperatoren
// ==============================

// ----- Test auf Gleichheit "==" -----

template <typename T>
bool mapra::operator==(const mapra::Matrix<T>& x, const mapra::Matrix<T>& y) {
	if (x.rows_ != y.rows_)
		return false;
	if (x.cols_ != y.cols_)
		return false;

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t c = 0; c < x.cols_; c++) {
			if (x(r, c) != y(r, c))
				return false;
		}
	}

	return true;
}

// ----- Test auf Ungleichheit "!=" -----

template <typename T>
bool mapra::operator!=(const mapra::Matrix<T>& x, const mapra::Matrix<T>& y) {
	return !(x == y);
}

// ==============================
//      Matrix-Vektor Produkte
// ==============================

// ----- Multiplikation Matrix*Vektor -----

template <typename T>
mapra::Vector<T> mapra::operator*(const mapra::Matrix<T>& x, const mapra::Vector<T>& y) {
#ifndef NDEBUG
	if (x.cols_ != y.GetLength()) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Vector<T> result(x.rows_);

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t i = 0; i < x.cols_; i++) {
			result(r) += x(r, i) * y(i);
		}
	}

	return result;
}

// ----- Multiplikation Vektor*Matrix -----

template <typename T>
mapra::Vector<T> mapra::operator*(const mapra::Vector<T>& x, const mapra::Matrix<T>& y) {
#ifndef NDEBUG
	if (x.GetLength() != y.rows_) {
		mapra::Matrix<T>::MatError("Ungültige Dimensionen!");
	}
#endif

	mapra::Vector<T> result(y.cols_);

	for (std::size_t c = 0; c < y.cols_; c++) {
		for (std::size_t i = 0; i < y.rows_; i++) {
			result(c) += x(i) * y(i, c);
		}
	}

	return result;
}

// ==========================
//      Ein- und Ausgabe
// ==========================

// ----- Ausgabe "<<" -----

template <typename T>
std::ostream& mapra::operator<<(std::ostream& s, const mapra::Matrix<T>& x) {
	s << std::setiosflags(std::ios::right);

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t c = 0; c < x.cols_; c++) {
			s << "\n(" << std::setw(4) << r << ", " << std::setw(4) << c << ") " << x(r, c);
		}
	}

	return s << std::endl;
}

// ----- Eingabe ">>" -----

template <typename T>
std::istream& mapra::operator>>(std::istream& s, mapra::Matrix<T>& x) {
	std::cout << std::setiosflags(std::ios::right);

	for (std::size_t r = 0; r < x.rows_; r++) {
		for (std::size_t c = 0; c < x.cols_; c++) {
			std::cout << "\n(" << std::setw(4) << r << ", " << std::setw(4) << c << ") " << std::flush;
			s >> x(r, c);
		}
	}

	return s;
}

// ==========================
//      Fehlerbehandlung
// ==========================

// ----- Ausgabe der Fehlermeldung "str" -----

template <typename T>
void mapra::Matrix<T>::MatError(const char str[]) {
	std::cerr << "\nMatrixfehler: " << str << '\n' << std::endl;
	exit(1);
}
