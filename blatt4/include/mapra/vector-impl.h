// Copyright (c) 2022, The MaPra Authors.

#include "vector.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>
#include <iomanip>

// =======================
//      Konstruktoren
// =======================

// ----- Konstruktor -----

template <typename T>
mapra::Vector<T>::Vector(std::size_t len) : elems_(len, 0) {}

template <typename T>
mapra::Vector<T>::Vector(const mapra::Vector<T>& vector) : elems_(vector.GetLength(), 0) {
	(*this) = vector;
}

// ===========================================
//      Vektorelement schreiben und lesen
// ===========================================

// ----- Schreib- und Lesezugriff auf Vektorelemente -----

template <typename T>
T& mapra::Vector<T>::operator()(std::size_t i) {
#ifndef NDEBUG
	if (i >= (*this).GetLength()) {
		mapra::Vector<T>::VecError("Ungueltiger Index!");
	}
#endif

	return elems_[i];
}

// ----- Lesezugriff auf Elemente konstanter Vektoren -----

template <typename T>
T mapra::Vector<T>::operator()(std::size_t i) const {
#ifndef NDEBUG
	if (i >= (*this).GetLength()) {
		mapra::Vector<T>::VecError("Ungueltiger Index!");
	}
#endif

	return elems_[i];
}

// =====================
//      Zuweisungen
// =====================

// ----- Zuweisungsoperator "=" ----

template <typename T>
mapra::Vector<T>& mapra::Vector<T>::operator=(const mapra::Vector<T>& other) {
	if ((*this).GetLength() != other.GetLength())
		Redim(other.GetLength());

	for (std::size_t i = 0; i < (*this).GetLength(); i++)
		(*this)(i) = other(i);

	return *this;
}

// ----- Zuweisungsoperator mit Addition "+=" ----

template <typename T>
mapra::Vector<T>& mapra::Vector<T>::operator+=(const mapra::Vector<T>& other) {
#ifndef NDEBUG
	if ((*this).GetLength() != other.GetLength()) {
		mapra::Vector<T>::VecError("Inkompatible Dimensionen fuer 'Vektor += Vektor'!");
	}
#endif

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) += other(i);
	}

	return *this;
}

// ----- Zuweisungsoperator mit Subtraktion "-=" ----

template <typename T>
mapra::Vector<T>& mapra::Vector<T>::operator-=(const mapra::Vector<T>& other) {
#ifndef NDEBUG
	if ((*this).GetLength() != other.GetLength()) {
		mapra::Vector<T>::VecError("Inkompatible Dimensionen fuer 'Vektor -= Vektor'!");
	}
#endif

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) -= other(i);
	}

	return *this;
}

// ----- Zuweisungsoperator mit Multiplikation "*=" ----

template <typename T>
mapra::Vector<T>& mapra::Vector<T>::operator*=(T c) {
	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) *= c;
	}

	return *this;
}

// ----- Zuweisungsoperator mit Divsion "/=" ----

template <typename T>
mapra::Vector<T>& mapra::Vector<T>::operator/=(T c) {
	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		(*this)(i) /= c;
	}

	return *this;
}

// ==============================
//      Vektorlaenge aendern
// ==============================

// ----- Vektorlaenge aendern -----

template <typename T>
mapra::Vector<T>& mapra::Vector<T>::Redim(std::size_t l) {
	elems_ = std::vector<T>(l, 0);
	return *this;
}

template <typename T>
std::size_t mapra::Vector<T>::GetLength() const { return elems_.size(); }

// ======================
//      Vektornormen
// ======================

// ----- Euklidische Norm -----

template <typename T>
T mapra::Vector<T>::Norm2() const {
	T res = 0;

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		res += (*this)(i) * (*this)(i);
	}

	return std::sqrt(res);
}

// ----- Maximum-Norm -----

template <typename T>
T mapra::Vector<T>::NormMax() const {
	T highscore = std::abs((*this)(0));

	for (std::size_t i = 0; i < (*this).GetLength(); i++){
		if (highscore <= std::abs((*this)(i)))
			highscore = std::abs((*this)(i));
	}

	return highscore;
}

// ==================================
//      arithmetische Operatoren
// ==================================

// ----- Addition "+" -----

template <typename T>
mapra::Vector<T> mapra::operator+(const mapra::Vector<T>& x, const mapra::Vector<T>& y) {
#ifndef NDEBUG
	if (x.GetLength() != y.GetLength()) {
		mapra::Vector<T>::VecError("Inkompatible Dimensionen fuer 'Vektor + Vektor'!");
	}
#endif

	mapra::Vector<T> z = x;
	return z += y;
}

// ----- Subtraktion "-" -----

template <typename T>
mapra::Vector<T> mapra::operator-(const mapra::Vector<T>& x, const mapra::Vector<T>& y) {
#ifndef NDEBUG
	if (x.GetLength()  != y.GetLength()) {
		mapra::Vector<T>::VecError("Inkompatible Dimensionen fuer 'Vektor + Vektor'!");
	}
#endif

	mapra::Vector<T> z = x;
	return z -= y;
}

// ----- Vorzeichen wechseln "-" -----

template <typename T>
mapra::Vector<T> mapra::operator-(const mapra::Vector<T>& x) {
	mapra::Vector<T> z = Vector(x.GetLength());

	return z - x;
}

// ----- Skalarprodukt "*" -----

template <typename T>
T mapra::operator*(const mapra::Vector<T>& x, const mapra::Vector<T>& y) {
#ifndef NDEBUG
	if (x.GetLength() != y.GetLength()) {
		mapra::Vector<T>::VecError("Skalarprodukt ungleiche Vektorlaenge");
	}
#endif

	T res = 0;

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		res += x(i) * y(i);
	}

	return res;
}

// ----- Multiplikation Skalar*Vektor "*" -----

template <typename T>
mapra::Vector<T> mapra::operator*(T c, const mapra::Vector<T>& x) {
	mapra::Vector<T> z = x;
	return z *= c;
}

// ----- Multiplikation Vektor*Skalar "*" -----

template <typename T>
mapra::Vector<T> mapra::operator*(const mapra::Vector<T>& x, T c) {
	return c * x;
}

// ----- Division Vektor/Skalar "/" -----

template <typename T>
mapra::Vector<T> mapra::operator/(const mapra::Vector<T>& x, T c) {
	mapra::Vector<T> z = x;
	return z /= c;
}

// ==============================
//      Vergleichsoperatoren
// ==============================

// ----- Test auf Gleichheit "==" -----

template <typename T>
bool mapra::operator==(const mapra::Vector<T>& x, const mapra::Vector<T>& y) {
	if (x.GetLength() != y.GetLength()) {
		return false;
	}

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		if (x(i) != y(i)) {
			return false;
		}
	}

	return true;
}

// ----- Test auf Ungleichheit "!=" -----

template <typename T>
bool mapra::operator!=(const mapra::Vector<T>& x, const mapra::Vector<T>& y) {
	return !(x == y);
}

// ==========================
//      Ein- und Ausgabe
// ==========================

// ----- Ausgabe "<<" -----

template <typename T>
std::ostream& mapra::operator<<(std::ostream& s, const mapra::Vector<T>& x) {
	s << std::setiosflags(std::ios::right);

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		s << "\n(" << std::setw(4) << i << ") " << x(i);
	}

	return s << std::endl;
}

// ----- Eingabe ">>" -----

template <typename T>
std::istream& mapra::operator>>(std::istream& s, mapra::Vector<T>& x) {
	std::cout << std::setiosflags(std::ios::right);

	for (std::size_t i = 0; i < x.GetLength(); i++) {
		std::cout << "\n(" << std::setw(4) << i << ") " << std::flush;
		s >> x(i);
	}

	return s;
}

// ==========================
//      Fehlerbehandlung
// ==========================

// ----- Ausgabe der Fehlermeldung "str" -----

template <typename T>
void mapra::Vector<T>::VecError(const char str[]) {
	std::cerr << "\nVektorfehler: " << str << '\n' << std::endl;
	exit(1);
}
