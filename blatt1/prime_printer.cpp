#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>

#include "prime_printer.h"

bool checkIfPrime(
		const std::vector<int>& primes, int nextToCheck,
		std::vector<int>& multiplesCache, int checkCount) {
	for (int checkIndex = 1; checkIndex < checkCount; checkIndex++) {
		while (multiplesCache[checkIndex] < nextToCheck)
			multiplesCache[checkIndex] += primes[checkIndex] + primes[checkIndex];

		if (multiplesCache[checkIndex] == nextToCheck) {
			return false;
		}
	}
	return true;
}

std::vector<int> calculatePrimes(int primeCount) {
	std::vector<int> primes(primeCount);
	primes[0] = 2;

	int nextToCheck = 1;
	int square = 9;

	int checkCount = 1;
	std::vector<int> multiplesCache;
	multiplesCache.push_back(0);

	for (int primeIndex = 1; primeIndex < primeCount; primeIndex++) {
		while (true) {
			nextToCheck += 2;
			if (nextToCheck == square) {
				checkCount++;
				square = primes[checkCount] * primes[checkCount];
				multiplesCache.push_back(nextToCheck);
			}

			if (checkIfPrime(primes, nextToCheck, multiplesCache, checkCount))
				break;
		}
		primes[primeIndex] = nextToCheck;
	}

	return primes;
}

void printRow(const std::vector<int>& primes, int primeCount, int row, int rows, int cols) {
	for (int column = 0; column < cols; column++)
		if (row + column * rows < primeCount)
			std::cout << std::setw(10) << primes[row + column * rows];

	std::cout << std::endl;
}

void printPage(const std::vector<int>& primes,
		int primeCount, int rows, int cols, int pageNumber, int pageOffset) {
	std::cout << "The First ";
	std::cout << primeCount;
	std::cout << " Prime Numbers --- Page ";
	std::cout << pageNumber;
	std::cout << std::endl;

	for (int row = pageOffset; row < pageOffset + rows; row++) {
		printRow(primes, primeCount, row, rows, cols);
	}

	std::cout << "\f" << std::endl;
}

void printPrimes(const std::vector<int>& primes, int rows, int cols) {
	int primeCount = primes.size();
	int pageNumber = 1;
	int pageOffset = 0;

	while (pageOffset < primeCount) {
		printPage(primes, primeCount, rows, cols, pageNumber, pageOffset);
		pageNumber++;
		pageOffset += rows * cols;
	}
}

void print() {
	const int kPrimeCount = 300;
	const int kRows = 50;
	const int kCols = 4;

	std::vector<int> primes = calculatePrimes(kPrimeCount);
	printPrimes(primes, kRows, kCols);
}
